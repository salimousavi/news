<?php

namespace Drupal\news\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Created by PhpStorm.
 * User: kasper
 * Date: 1/31/17
 * Time: 11:11 AM
 */
class NewsController {

    public function index() {

        $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties([
                'vid'               => 'news',
                'field_news_enable' => true,
            ]);

        usort($terms, function ($a, $b) {
            if ($a->getWeight() == $b->getWeight()) {
                return 0;
            }

            return ($a->getWeight() > $b->getWeight()) ? -1 : 1;
        });

        $result = ['en' => [], 'fa' => []];
        foreach ($terms as $term) {
            if ($term->language()->getId() == 'en') {
                $result['en'][] = array(
                    'text' => $term->get('field_news')->getValue()[0]['value'],
                );
            }
            else {
                $result['fa'][] = array(
                    'text' => $term->get('field_news')->getValue()[0]['value'],
                );
            }
        }

        return new JsonResponse($result);
    }

}