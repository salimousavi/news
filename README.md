# News

<h3> Installation </h3>

Download this module and add in module directory and enable it.

<h3> How to work? </h3>
This module create a vocabulary that named <code>news</code>.<br>

<b>Fields:</b>
 <br><code> field_news_enable </code>
 <br><code> field_news </code>

<b>Route request:</b>
 <br>
 <code> api/news </code>

<b>Response:</b>
 <br>
 <code>text</code> 
 
 
